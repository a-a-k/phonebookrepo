﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Models;
using PhoneBook.Services;

namespace PhoneBook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly SQLiteConnection connection;
        private readonly IdentityService identity;

        public PeopleController(SQLiteConnection connection, IdentityService identity)
        {
            this.connection = connection;
            this.identity = identity;
        }

        // GET: api/People
        [HttpGet]
        public IActionResult GetPersons()
        {
            var persons = new List<Person>();
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT * FROM Persons WHERE UserId = {identity.CurrentUserId};";
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var p = new Person
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            Phone = reader.GetString(2),
                        };

                        persons.Add(p);
                    }
                }
            }

            return Ok(persons);
        }

        // GET: api/People/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPerson([FromRoute]string id)
        {
            Person person = null;
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT * FROM Persons WHERE Id = {id};";
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        person = new Person
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            Phone = reader.GetString(2),
                        };
                    }
                }
            }

            if (person == null)
            {
                return NotFound();
            }

            return Ok(person);
        }

        // PUT: api/People/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutPerson([FromRoute]int id, [FromBody]Person person)
        {
            if (id != person.Id)
            {
                return BadRequest();
            }

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"UPDATE Persons SET Name = '{person.Name}', Phone = '{person.Phone}' WHERE Id = {id};";
                await cmd.ExecuteNonQueryAsync();
            }

            return NoContent();
        }

        // POST: api/People
        [HttpPost]
        public async Task<IActionResult> PostPerson([FromBody]Person person)
        {
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"INSERT INTO Persons (Name, Phone, UserId) VALUES ('{person.Name}', '{person.Phone}', {identity.CurrentUserId});";
                await cmd.ExecuteNonQueryAsync();
            }

            return NoContent();
        }

        // DELETE: api/People/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeletePerson([FromRoute]int id)
        {
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = $"DELETE FROM Persons WHERE Id = {id};";
                await cmd.ExecuteNonQueryAsync();
            }

            return NoContent();
        }
    }
}