using System.Data.SQLite;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Models;
using PhoneBook.Services;

namespace PhoneBook.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SQLiteConnection connection;
        private readonly IdentityService identity;

        public AccountController(SQLiteConnection connection, IdentityService identity)
        {
            this.connection = connection;
            this.identity = identity;
        }

        [HttpPost]
        public int Authenticate([FromBody]User user)
        {
            using var cmd = connection.CreateCommand();
            cmd.CommandText = $"SELECT Id FROM Users WHERE Username = '{user.Username}' AND Password = '{user.Password}';";
            using var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var id = reader.GetInt32(0);
                identity.CurrentUserId = id;
                return id;
            }

            return 0;
        }
    }
}