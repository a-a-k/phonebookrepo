﻿using System.Data.SQLite;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Services;

namespace PhoneBook
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AppPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddMvc();
            var connection = new SQLiteConnection(Configuration.GetConnectionString("DefaultConnection"));
            services.AddSingleton(connection);
            services.AddSingleton(new IdentityService());

            FillDb(connection);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors();
            app.UseMvc();
        }

        private static void FillDb(SQLiteConnection con)
        {
            con.Open();
            using (var cmd = new SQLiteCommand(con))
            {
                #region users
                cmd.CommandText = "DROP TABLE IF EXISTS Users;";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE Users(Id INTEGER PRIMARY KEY, Username TEXT, Password TEXT);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Users(Username, Password) VALUES('user1','user1');";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Users(Username, Password) VALUES('user2','user2');";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Users(Username, Password) VALUES('user3','user3');";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Users(Username, Password) VALUES('user4','user4');";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Users(Username, Password) VALUES('user5','user5');";
                cmd.ExecuteNonQuery();
                #endregion

                #region persons
                cmd.CommandText = "DROP TABLE IF EXISTS Persons;";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE Persons(Id INTEGER PRIMARY KEY, Name TEXT, Phone TEXT, UserId INTEGER);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Sam',52642,1);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Ivan',57127,1);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Boris',9000,1);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Vova',29000,2);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Kevin',350000,2);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Doctor',21000,3);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Ruslan',41400,4);";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO Persons(Name, Phone, UserId) VALUES('Mom',21600,5);";
                cmd.ExecuteNonQuery();
                #endregion
            }
        }
    }
}
