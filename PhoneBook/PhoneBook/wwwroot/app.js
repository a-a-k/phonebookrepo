﻿var app = angular.module("phoneBook", ['ui.bootstrap']);

app.controller("loginController", ['$scope', '$http', '$window', function ($scope, $http, $window) {
    $scope.formSubmit = function () {
        $http({
            method: 'POST',
            url: '/api/Account',
            data: {username: $scope.username, password: $scope.password}
        }).then(function (response) {
            if(response.data > 0){
                $window.localStorage.setItem('isAuthorized', 'true');
                $window.location.href = '/';
            }
        }, function (error) {
            console.log(error);
        });
    };
}]);

app.controller("peopleController", ['$scope', '$http', '$window', function ($scope, $http, $window) {

    $scope.personsList = null;
    $scope.sortBy = 'name';
    $scope.reverseBy = false;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 2;
    $scope.totalItems = 0;
    let allData = [];

    Array.prototype.skip = function (count) {
        return this.filter((_, i) => i >= count);
    };

    Array.prototype.take = function (count) {
        return this.filter((_, i) => i < count);
    };

    function checkIfAuthorized() {
        const value = $window.localStorage.getItem('isAuthorized');
        if (!value || value !== 'true')
            $window.location.href = '/login.html';
    }

    getAllData();

    function getAllData() {

        checkIfAuthorized();

        $http({
            method: 'GET',
            url: '/api/People'
        }).then(function (response) {
            allData = response.data;
            $scope.totalItems = allData.length;
            $scope.personsList = allData.take($scope.itemsPerPage);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getPerson = function (person) {

        checkIfAuthorized();

        $http({
            method: 'GET',
            url: '/api/People/' + parseInt(person.id)
        }).then(function (response) {
            $scope.personModel = response.data;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.addPerson = function () {

        checkIfAuthorized();

        $http({
            method: 'POST',
            url: '/api/People',
            data: $scope.personModel
        }).then(function (response) {
            getAllData();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.editPerson = function () {

        checkIfAuthorized();

        $http({
            method: 'PUT',
            url: '/api/People/' + parseInt($scope.personModel.id),
            data: $scope.personModel
        }).then(function (response) {
            getAllData();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.deletePerson = function (person) {

        checkIfAuthorized();

        $http({
            method: 'DELETE',
            url: '/api/People/' + parseInt(person.id)
        }).then(function (response) {
            getAllData();
        }, function (error) {
            console.log(error);
        });
    };

    $scope.clear = function () {
        $scope.personModel = {};
        $scope.personModel.name = '';
        $scope.personModel.phone = '';
    };

    $scope.pageChanged = function () {
        $scope.personsList = allData.skip(($scope.currentPage-1) * $scope.itemsPerPage).take($scope.itemsPerPage);
    };
}])